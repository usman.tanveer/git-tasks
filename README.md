# Students Record


## Working

This is a program that stores **name**, **rollno** and **cgpga** of a specific number of students.

User can:
- **add** a new student
- **delete** an existing student
- **display** all the students in class
